/**
 * Created by Dmitry on 6/7/2015.
 */
public class Odometer {
    public final int MAX_MILEAGE = 999999;
    public final int MPG = 24;
    private int mileage;
    private int setPoint;
    private FuelGauge fuelGauge;

    public Odometer(int m, FuelGauge fg){
        mileage = m;
        fuelGauge = fg;
    }

    public int getMileage(){
        return mileage;
    }

    public void incrementMileage(){
        if (mileage < MAX_MILEAGE) {
            mileage++;
            setPoint++;
        } else {
            mileage = 0;
            setPoint++;
        }

        if (setPoint == MPG) {
            fuelGauge.decrementGallons();
            setPoint = 0;
        }
    }
}
