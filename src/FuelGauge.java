/**
 * Created by Dmitry on 6/7/2015.
 */
public class FuelGauge {
    public final int MAX_GALLONS = 15;
    private int gallons;

    public FuelGauge(){
        gallons = 0;
    }

    public FuelGauge(int g){
        if (gallons <= MAX_GALLONS)
            gallons = g;
        else
            gallons = MAX_GALLONS;
    }

    public int getGallons() {
        return gallons;
    }

    public void incrementGallons(){
        if (gallons < MAX_GALLONS)
            gallons++;
    }

    public void decrementGallons(){
        if (gallons > 0)
            gallons--;
    }
}
